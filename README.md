## Tigefa Builder [![pipeline status](https://gitlab.com/tigefa/builder/badges/master/pipeline.svg)](https://gitlab.com/tigefa/builder/commits/master) [![coverage report](https://gitlab.com/tigefa/builder/badges/master/coverage.svg)](https://gitlab.com/tigefa/builder/commits/master)

container registry for build electron projects :truck:

use this image

```
image: registry.gitlab.com/tigefa/builder:latest
```
